<%-- 
    Document   : stats
    Created on : May 28, 2014, 8:33:57 PM
    Author     : benj
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Stats Page</title>
    </head>
    <body>
        <%
            int[] count = (int[]) request.getSession().getAttribute("pages");
        %>

        <h2>The number of hits to <a href="./Controller?page=page1">Page 1</a> is <%=count[1]%> </h2>
        <h2>The number of hits to <a href="./Controller?page=page2">Page 2</a> is <%=count[2]%> </h2>
        <h2>The number of hits to <a href="./Controller?page=page3">Page 3</a> is <%=count[3]%> </h2>
        <br>
        <h2>Number of hits to this page  are <%=count[0]%> </h2> 
        <a href="./index.html">Back to home page</a>
    </body>
</html>
