/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ed.wmich;

import java.io.Serializable;
import java.util.ArrayList;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author benj
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable{

    /**
     * Creates a new instance of LoginBean
     */
    
    private String userName;
    private String userPass;
    private String make;
    private String model;
    private int year;
    private String vaildUserName="test";
    private final String vaildUserPass="test";
    private boolean authed=false;
    private ArrayList<Vehicle> allVehicles = new ArrayList<Vehicle>();
    
    
    
    
    
    public LoginBean() {
        allVehicles.add(new Vehicle("chevy","car", 1992));
        allVehicles.add(new Vehicle("ford","mustang", 1968));
        allVehicles.add(new Vehicle("toyota","airplane", 1923));
        allVehicles.add(new Vehicle("chevy","not a airplane", 1963));
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the userPass
     */
    public String getUserPass() {
        return userPass;
    }

    /**
     * @param userPass the userPass to set
     */
    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    /**
     * @return the vaildUserName
     */
    public String getVaildUserName() {
        return vaildUserName;
    }

    /**
     * @param vaildUserName the vaildUserName to set
     */
    public void setVaildUserName(String vaildUserName) {
        this.vaildUserName = vaildUserName;
    }

    /**
     * @return the vailsUserPass
     */
    public String getVaildUserPass() {
        return vaildUserPass;
    }

    public String addVehicle()
    {
     allVehicles.add(new Vehicle(make, model, year));
     return "main";
    }

    /**
     * @return the authed
     */
    public boolean isAuthed() {
        return authed;
    }

    /**
     * @param authed the authed to set
     */
    public void setAuthed(boolean authed) {
        this.authed = authed;
    }
    public String login()
    {
        if(userName.equals(vaildUserName) && userPass.equals(vaildUserPass)){
            authed=true;
            return "main";
        }
        else{
            authed=false;
            return "login";
        }
    }
    public String logOut()
    {
        authed = false;
        return "login";
    }

    /**
     * @return the allVehicles
     */
    public ArrayList<Vehicle> getAllVehicles() {
        return allVehicles;
    }

    /**
     * @param allVehicles the allVehicles to set
     */
    public void setAllVehicles(ArrayList<Vehicle> allVehicles) {
        this.allVehicles = allVehicles;
    }
    public String deleteVehicle(Vehicle vehicle)
    {
        allVehicles.remove(vehicle);
        return "main";
    }

    /**
     * @return the make
     */
    public String getMake() {
        return make;
    }

    /**
     * @param make the make to set
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }
}
