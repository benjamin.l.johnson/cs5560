/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wmich;


import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import sun.net.www.protocol.http.HttpURLConnection;

/**
 *
 * @author happy
 */
public class Weather
{

	String key = "6b286f19fe6fa1cd5baba60e17e83487fd274f80";
	String serviceName = "http://api.worldweatheronline.com/free/v1/weather.ashx?key=";
	String query = "&q=";
	String format = "&format=json";
	String zip;

	public Weather(String key, String serviceName, String zip, String format)
	{
		this.key = key;
		this.serviceName = serviceName;
		this.zip = zip;
		this.format = format;

	}

	public String getCurrentTemp()
	{
		String result = "";
		try
		{

			URL url = new URL(serviceName + key + query + zip + format);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200)
			{
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
			InputStream is = url.openStream();
			JsonReader reader = Json.createReader(is);

			JsonObject obj = reader.readObject();

			JsonArray arr = obj.getJsonObject("data").getJsonArray("current_condition");

			result = arr.getJsonObject(0).getString("temp_F", "Ben");

			is.close();

		} catch (MalformedURLException e)
		{

			e.printStackTrace();

		} catch (IOException e)
		{

			e.printStackTrace();

		} catch (NullPointerException e)
		{
			e.printStackTrace();
		}
		
		
		return result.isEmpty() ? "an error occured" : result;
	}
}
