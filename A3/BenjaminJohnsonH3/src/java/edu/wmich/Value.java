/*
 * This is taken mostly from the class the only thing I did 
 * different was I get the favicon from the site it works with
 * sites like youtube and google if you input the url
 * correctly 

*/
package edu.wmich;


/**
 *
 * @author Benjamin Johnson
 */
public class Value
{

    private String siteUserName;
    private String url;
    private String passwd;
    private String icon;

    public Value(String url, String passwd, String siteUserName)
    {
        this.url = url;
        this.passwd = passwd;
        this.siteUserName = siteUserName;
        setIcon(url);
    }

    /**
     * @return the url
     */
    public String getUrl()
    {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url)
    {
        this.url = url;
    }

    /**
     * @return the password
     */
    public String getPasswd()
    {
        return passwd;
    }

    /**
     * @param password the password to set
     */
    public void setPasswd(String passwd)
    {
        this.passwd = passwd;
    }

    /**
     * @return the icon
     */
    public String getIcon()
    {
        return icon;
    }

    /**
     * @param Takes the url and gets the favicon for that site only tested with google.com
     */
    public void setIcon(String url)
    {
        if(!url.startsWith("http://"))
        {
          url = "http://"+url;
        }
        
        if(url.endsWith("/"))
        {
          url = url+"favicon.ico";
        }
        else
        {
           url = url+"/favicon.ico";
        }
        this.icon = url;
    }

    /**
     * @return the siteUserName
     */
    public String getSiteUserName()
    {
        return siteUserName;
    }

    /**
     * @param siteUserName the siteUserName to set
     */
    public void setSiteUserName(String siteUserName)
    {
        this.siteUserName = siteUserName;
    }

}
