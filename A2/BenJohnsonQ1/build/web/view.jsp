<%-- 
    Document   : newjsp
    Created on : May 15, 2014, 12:02:54 AM
    Author     : happy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <title>js</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    </head>
    <body>  
        <%
            //this way on every get request to this page we can init a new session
            session.setAttribute("board", null);
        %>
        <canvas id="myCanvas" width="400" height="400" style="border:1px solid #000000;">

        </canvas>
       
        <script src="js/libs/jquery/jquery.js" type="text/javascript"></script>
        <script src="canvas.js" type="text/javascript"></script>
    </body>

</html>
