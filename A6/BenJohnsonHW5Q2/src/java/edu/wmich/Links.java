/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wmich;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author happy
 */
@Entity
@Table(name = "LINKS")
@XmlRootElement
@NamedQueries(
{
	@NamedQuery(name = "Links.findAll", query = "SELECT l FROM Links l"),
	@NamedQuery(name = "Links.findByUrl", query = "SELECT l FROM Links l WHERE l.url = :url"),
	@NamedQuery(name = "Links.findByUserName", query = "SELECT l FROM Links l WHERE l.userName = :userName"),
	@NamedQuery(name = "Links.findByPassword", query = "SELECT l FROM Links l WHERE l.password = :password")
})
public class Links implements Serializable
{
	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "URL")
	private String url;
	@Size(max = 64)
    @Column(name = "USER_NAME")
	private String userName;
	@Size(max = 64)
    @Column(name = "PASSWORD")
	private String password;

	public Links()
	{
	}

	public Links(String url)
	{
		this.url = url;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (url != null ? url.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Links))
		{
			return false;
		}
		Links other = (Links) object;
		if ((this.url == null && other.url != null) || (this.url != null && !this.url.equals(other.url)))
		{
			return false;
		}
		return true;
	}
	
	@Override
	public String toString()
	{
		return "edu.wmich.Links[ url=" + url + " ]";
	}
	
	private String loginError()
	{
		return "{\"error\":\"need to login\"}";
	}
	
}
