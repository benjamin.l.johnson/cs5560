/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wmich;

/**
 *
 * @author Ben Johnson
 */
public class Converter
{

    private double num;
    private String to;
    private String from;
    private String toOutPut;
    private String fromOutPut;
    private String finalResult="Select a unit to convert from and to!";
    private String eqn="";

    /**
     * @return the to
     */
    public String getTo()
    {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(String to)
    {
        this.to = to;
    }

    /**
     * @return the from
     */
    public String getFrom()
    {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from)
    {
        this.from = from;
    }

    /**
     * @return the toOutPut
     */
    public String getToOutPut()
    {
        return toOutPut;
    }

    /**
     * @param toOutPut the toOutPut to set
     */
    public void setToOutPut(String toOutPut)
    {
        this.toOutPut = toOutPut;
    }

    /**
     * @return the fromOutPut
     */
    public String getFromOutPut()
    {
        return fromOutPut;
    }

    /**
     * @param fromOutPut the fromOutPut to set
     */
    public void setFromOutPut(String fromOutPut)
    {
        this.fromOutPut = fromOutPut;
    }

    /**
     * @return the finalResult
     */
    public String getFinalResult()
    {
        return finalResult;
    }

    /**
     * @param finalResult the finalResult to set
     */
    public void setFinalResult(String finalResult)
    {
        this.finalResult = finalResult;
    }

    public void convert()
    {
        //making the little [°C] symbols 
        setToOutPut("[°" + to.toUpperCase() + "]");
        setFromOutPut("[°" + from.toUpperCase() + "]");
        switch (from)
        {
            case "C":
                cel();
                break;

            case "F":
                far();
                break;

            case "K":
                kelv();
                break;

            case "R":
                rank();
                break;
        }
        

    }
    
    //convert from Celsius to ...
    private void cel()
    {
        double res=0;
        switch (to)
        {
            case "F":
                res = num * (9.0 / 5) + 32;
                eqn = " × 9⁄5 + 32";
                break;
            case "K":
                res = num + 273.15;
                eqn = " + 273.15";
                break;
            case "R":
                res = (num + 273.15) * (9.0 / 5);
                eqn = " + 273.15 × 9⁄5";
                break;
            default:
                res = num;
                eqn ="";
                break;
        }
        
        //prints the whole output (i.e 0°C=32°F)
        setFinalResult(String.format("%.2f%s = %.2f%s",num,fromOutPut.substring(1,3)
                ,res,toOutPut.substring(1,3 )));
        
        
        eqn=toOutPut+ "=" + fromOutPut +eqn;
    }
    
    //Convert from Fahrenheit to ...
    private void far()
    {
        double res=0;
        switch (to)
        {
            case "C":
                //Celsius [°C] = ([°F] − 32) × 5⁄9
                res = (num - 32) * (5 / 9.0);
                eqn = " - 32 * (5 / 9)";
                break;
            case "K":
                //Kelvin [K] = ([°F] + 459.67) × 5⁄9
                res = (num + 459.67) * (5 / 9.0);
                eqn = " + 459.67 * (5/9)";
                break;
            case "R":
                //Rankine [°R] = [°F] + 459.67 
                res = num + 459.67;
                eqn = " + 459.67";
                break;
            default:
                res = num;
                eqn ="";
                break;
        }
        //prints the whole output (i.e 0°C=32°F)
        setFinalResult(String.format("%.2f%s = %.2f%s",num,fromOutPut.substring(1,3)
                ,res,toOutPut.substring(1,3 )));
        eqn=toOutPut+ "=" + fromOutPut +eqn;
    }
    
    //Convert from Kelvin to ... 
    private void kelv()
    {
        double res=0;
        switch (to)
        {
            case "F":
                //[°F] = [K] × 9⁄5 − 459.67
                res = num * (9.0 / 5) - 459.67;
                eqn = " × 9⁄5 - 459.67";
                break;
            case "C":
                res = num - 273.15;
                eqn = " - 273.15";
                break;
            case "R":
                //[°R] = [K] × 9⁄5
                res = num * (9.0 / 5);
                eqn = " × 9⁄5";
                break;
            default:
                res = num;
                eqn = "";
                break;
        }
        //prints the whole output (i.e 0°C=32°F)
        setFinalResult(String.format("%.2f%s = %.2f%s",num,fromOutPut.substring(1,3)
                ,res,toOutPut.substring(1,3 )));
        eqn=toOutPut+ "=" + fromOutPut +eqn;

    }
    //convert from Rankine to ...
    private void rank()
    {
        double res=0;
        switch (to)
        {
            case "F":
                //[°F] = [°R] − 459.67
                res = num - 459.67;
                eqn = " - 459.67";
                break;
            case "C":
                //[°C] = ([°R] − 491.67) × 5⁄9
                res = (num - 491.67) * (5.0 / 9);
                eqn = " - 491.67 * (5/9)";
                break;
            case "K":
                //[K] = [°R] × 5⁄9
                res = num * (5.0 / 9);
                eqn = " × 5/9";
                break;
            default:
                res = num;
                eqn = "";
                break;
        }
        //prints the whole output (i.e 0°C=32°F)
        setFinalResult(String.format("%.2f%s = %.2f%s",num,fromOutPut.substring(1,3)
                ,res,toOutPut.substring(1,3 )));
        eqn=toOutPut+ "=" + fromOutPut +eqn;
    }

    /**
     * @return the num
     */
    public double getNum()
    {
        return num;
    }

    /**
     * @param num the num to set
     */
    public void setNum(double num)
    {
        this.num = num;
    }

    /**
     * @return the eqn
     */
    public String getEqn()
    {
        return eqn;
    }

    /**
     * @param eqn the eqn to set
     */
    public void setEqn(String eqn)
    {
        this.eqn = eqn;
    }

}
