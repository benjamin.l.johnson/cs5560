/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wmich;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.GsonBuilder;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author happy
 */
public class Controller extends HttpServlet
{

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		response.setContentType("application/json;charset=UTF-8");
		PrintWriter out = response.getWriter();


		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		Gson gson = new GsonBuilder().create();
		//response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
		if (user == null)
		{
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);

			out.println("{\"login\":\"" + userService.createLoginURL("/") + "\"}");

			//_ah/logout?continue=%2Fapp
			//response.sendRedirect(userService.createLoginURL(request.getRequestURI()));

		} else
		{
			try
			{
				DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

				//GsonBuilder gson = new GsonBuilder();

				if (request.getMethod().equals("POST"))
				{

					String userName = request.getParameter("userName");
					String url = request.getParameter("url");
					String password = request.getParameter("password");
					Key siteKey = KeyFactory.createKey("Site", url);
					Entity site = new Entity(siteKey);
					site.setProperty("userName", userName);
					site.setProperty("password", password);
					Transaction tran = ds.beginTransaction();
					ds.put(site);
					tran.commit();
					response.setStatus(200);
					out.println("{\"success\":\"Url" + url + " added successfully\"}");
				} else if (request.getMethod().equals("DELETE"))
				{
					//site.getProperty("url");
					String url = request.getParameter("url");

					Key siteKey = KeyFactory.createKey("Site", url);
					Entity e = ds.get(siteKey);
					ds.delete(siteKey);
					out.println("{\"success\":\"Site with " + url + " deleted\"}");


				} else if (request.getMethod().equals("GET"))
				{


					DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
					//Key siteKey = KeyFactory.createKey("Site");
					// Run an ancestor query to ensure we see the most up-to-date
					// view of the Greetings belonging to the selected Guestbook.
					Query query = new Query("Site");
					List<Entity> list = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(5));

					out.print(gson.toJson(list));


				} else
				{
					response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
					out.println("{\"error\":\"Method not supported\"}");
				}

			} catch (Exception e)
			{
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				out.println("{\"error\":\"" + e.getMessage() + "\"}");
				e.printStackTrace();
			} finally
			{
				out.close();
			}
		}
	}

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo()
	{
		return "Short description";
	}// </editor-fold>

}
