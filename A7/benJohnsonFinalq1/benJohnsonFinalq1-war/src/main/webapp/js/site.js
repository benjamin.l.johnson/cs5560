 /* 
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */

 //on page create do this 
 $(document).on("pageshow", "#page1", function() {
   
   //make the ajax call
   $.ajax({
     type: 'GET',
     url: '/app',
     dataType: 'json',
     success: function(data) {
       $('#content').empty();

       var len = data.length;
       for (var i = 0; i < len; i++)
       {
	 $('#content').append($('<div>')
		 .attr({
		   'data-role': 'collapsible',
		   'data-collapsed': 'true',
		   'id': i
		 }).html('<h4> Url: ' + data[i].key.name + '</h4>')
		 );

	 //make the collapsible set
	 $("#" + i).append($('<div>')
		 .attr({
		   'data-role': 'collapsible-set',
		   'data-content-theme': 'c',
		   'data-collapsed': 'true',
		   'id': 'collapse' + i
		 }));

	 //Append to the collapsible set 
	 $("#collapse" + i).append($('<div>')
		 .attr({
		   'data-role': 'collapsible',
		   'data-content-theme': 'c',
		   'data-collapsed': 'true'
		 })
		 .html('<h4>Username</h4><p>' + data[i].propertyMap.userName + ' </p>'));

	 //Append to the collapsible set 
	 $("#collapse" + i).append($('<div>')
		 .attr({
		   'data-role': 'collapsible',
		   'data-content-theme': 'c',
		   'data-collapsed': 'true'
		 })
		 .html('<h4>Password</h4><p>' + data[i].propertyMap.password + '</p>'));

	 //appending the data button
	 $("#collapse" + i).append($('<a>')
		 .attr({
		   'data-role': 'button',
		   'data-theme': 'a',
		   'data-icon': 'delete',
		   'value': data[i].key.name,
		   'id': 'deleteLink'
		 }).text("Delete"));
       }
       //create the stuff
       $('#page1').trigger('create');

     },
     error: function(jqXHR) {
       var response = $.parseJSON(jqXHR.responseText);
       window.location.replace(response.login);
       console.log(jqXHR.responseText);
     }
   });

 });

//function to change page
 $(document).on('tap', '#add', function(e) {
   $.mobile.changePage('#bar');
 });
 


 $(document).on('tap', '#deleteLink', function() {
   var url = $(this).attr("value");
   $.ajax({
     type: 'DELETE',
     url: '/app?url=' + url,
     dataType: 'json',
     success: function(result) {
       location.reload();
     },
     error: function(jqXHR) {
       console.log(jqXHR.responseText);
     }
   });

 });


 $(document).on('tap', '#save', function() {

   $.ajax({
     type: 'POST',
     url: '/app',
     dataType: 'json',
     data: {userName: $('#userName').val(), password: $('#password').val(), url: $('#url').val()},
     success: function(result) {
       //clear the form
       $('.userform')[0].reset();


       //change the page
       $.mobile.changePage("/");
     },
     error: function(jqXHR) {
       console.log(jqXHR.responseText);
     }
   });

 });
