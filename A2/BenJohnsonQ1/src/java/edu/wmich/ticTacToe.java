/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wmich;

import java.io.IOException;
import java.io.PrintWriter;
import javax.json.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author happy
 */
@WebServlet(name = "ticTacToe", urlPatterns =
{
    "/ticTacToe"
})
public class ticTacToe extends HttpServlet
{

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter())
        {
            /* TODO output your page here. You may use following sample code. */
            System.out.print("They pick Cell:"+ request.getParameter("number"));

            //Define the board object
            Board board;
            
            HttpSession session = request.getSession();
            
            try
            {
                if(request.getParameter("newGame").equals("true"))
                {
                    session.setAttribute("board", null);
                }
            }
            catch(NullPointerException e)
            {
                
            }
            
            
            if(session.getAttribute("board") == null)
            {
                 board = new Board();
                 System.err.println("New board");
                 
            }
            else
            {
                board =(Board) session.getAttribute("board");
            }
           
            //Lets get the cell number they want to move to
            int cellNum = Integer.parseInt(request.getParameter("number"));

            //Init JSON element
            JsonObjectBuilder obj = Json.createObjectBuilder();
            session.setAttribute("board", board);
            
            //Check to see if that cell number has anything in it
            if (board.cellEmpty(cellNum))
            {
                //X's=1 O's=0
                board.setCellValue(cellNum, 1);
                if (board.checkWin() > -1)
                {
                    System.err.println("WINNER!");
                    obj.add("won","yes");
                    obj.add("message",board.getWinner());
                    session.invalidate();
                    
                } 
                else
                {
                    //server is moving
                    int nextMove = board.calculateNextMove();
                    board.setCellValue(nextMove, 0);
                    if (board.checkWin() > -1)
                    {
                        System.err.println("WINNER!");
                        obj.add("won","yes");
                        obj.add("message",board.getWinner());
                        session.invalidate();
                        
                    }
                    
                    obj.add("move", nextMove);
                }
                obj.add("error", 0);
                
            } 
            else
            {
                obj.add("error", board.getCellValue(cellNum) - 2);
                System.out.println("ERROR " + (board.getCellValue(cellNum) - 2));
            }

            JsonObject done = obj.build();


            out.print(done.toString());
            out.flush();

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
         processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

}
