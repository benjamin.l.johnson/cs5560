'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
        .controller('MyCtrl1', ['$scope', function($scope) {
                //setting num1 to equal zero to start
                $scope.num1 = 0;
                
                //all the different types of 
                $scope.types = [
                    {value: 'Celsius', id: 'C'},
                    {value: 'Fahrenheit', id: 'F'},
                    {value: 'Kelvin', id: 'K'},
                    {value: 'Rankine', id: 'R'}
                ];
                
                //A object to hold data in
                $scope.data = {};

                
                $scope.convert = function()
                {

                    if ($scope.data.from === "C")
                    {
                        cel();
                    }
                    else if ($scope.data.from === "F")
                    {
                        far();
                    }
                    else if ($scope.data.from === "K")
                    {
                        kelv();
                    }
                    else if ($scope.data.from === "R")
                    {
                        rank();
                    }
                };
                
                 	
                 	
                //Fucntion to convert fahrenheit  
                function far()
                {
                    var to = $scope.data.to;
                    if (to === "C")
                    {
                        //Celsius [°C] = ([°F] − 32) × 5⁄9
                        $scope.num3 = ($scope.num1 - 32) * (5 / 9);
                        $scope.eqn = " - 32 * (5 / 9)";
                    }
                    else if (to === "K")
                    {
                        //Kelvin [K] = ([°F] + 459.67) × 5⁄9
                        $scope.num3 = ($scope.num1 + 459.67) * (5 / 9);
                        $scope.eqn = " + 459.67 * (5/9)";
                    }
                    else if (to === "R")
                    {
                        //Rankine [°R] = [°F] + 459.67 
                        $scope.num3 = $scope.num1 + 459.67;
                        $scope.eqn = " + 459.67";
                    }
                    else
                    {
                        $scope.num3 = $scope.num1;
                        $scope.eqn = "";
                    }
                }
                ;
                
                //function to convert from celsius
                function cel()
                {
                    var to = $scope.data.to;
                    if (to === "F")
                    {
                        $scope.num3 = $scope.num1 * (9 / 5) + 32;
                        $scope.eqn = " × 9⁄5 + 32";
                    }
                    else if (to === "K")
                    {
                        $scope.num3 = $scope.num1 + 273.15;
                        $scope.eqn = " + 273.15";
                    }
                    else if (to === "R")
                    {
                        $scope.num3 = ($scope.num1 + 273.15) * (9 / 5);
                        $scope.eqn = " + 273.15 × 9⁄5";
                    }
                    else
                    {
                        $scope.num3 = $scope.num1;
                        $scope.eqn = "";
                    }
                }
                ;
                
                //Convert from kelvin
                function kelv()
                {
                    var to = $scope.data.to;
                    if (to === "F")
                    {
                        //[°F] = [K] × 9⁄5 − 459.67
                        $scope.num3 = $scope.num1 * (9 / 5) - 459.67;
                        $scope.eqn = " × 9⁄5 - 459.67";
                    }
                    else if (to === "C")
                    {
                        $scope.num3 = $scope.num1 - 273.15;
                        $scope.eqn = " - 273.15";
                    }
                    else if (to === "R")
                    {
                        //[°R] = [K] × 9⁄5
                        $scope.num3 = $scope.num1 * (9 / 5);
                        $scope.eqn = " × 9⁄5";
                    }
                    else
                    {
                        $scope.num3 = $scope.num1;
                        $scope.eqn = "";
                    }
                }
                ;
                
                //Convert from Rankine
                function rank()
                {
                  var to = $scope.data.to;
                    
                    if (to === "F")
                    {
                        //[°F] = [°R] − 459.67
                        $scope.num3 = $scope.num1  - 459.67;
                        $scope.eqn = " - 459.67";
                    }
                    
                    
                    else if (to === "C")
                    {
                        //[°C] = ([°R] − 491.67) × 5⁄9
                        $scope.num3 = ($scope.num1 - 491.67) * (5/9);
                        $scope.eqn = " - 491.67 * (5/9)";
                    }
                    
                    else if (to === "K")
                    {
                        //[K] = [°R] × 5⁄9	
                        $scope.num3 = $scope.num1 * (5/9);
                        $scope.eqn = " × 5/9";
                    }
                    else
                    {
                        $scope.num3 = $scope.num1;
                        $scope.eqn = "";
                    }  
                };


            }])
        .controller('MyCtrl2', ['$scope', function($scope) {

            }]);
