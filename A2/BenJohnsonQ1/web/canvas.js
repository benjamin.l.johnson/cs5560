 /* 
  * Each box for the board is labeled accordingly 
  * 0 1 2 
  * 3 4 5
  * 6 7 8
  * 
  }*/
 
 
function winnerBox(message) {
    
    //if they want to play again reload the page
    if (confirm(message+"\nWant to play again?") === true) {
        location.reload();
    } else {
        
    }
}


//function to alert you when clicked already selected box
 function alreadyChecked(num)
 {

   if (num === -1)
   {
     alert("You already checked that box!");
   }
   else
   {
     alert("I already checked that box!");
   }
 }

 //gete the mouse position of the click on the canvas
 function getMousePos(canvas, evt) {
   var rect = canvas.getBoundingClientRect();
   return {
     x: evt.clientX - rect.left,
     y: evt.clientY - rect.top
   };
 }

 //Draws an x in a selected box
 function drawX(start, end)
 {
   context.beginPath();
   context.strokeStyle='black';
   context.moveTo(start + 20, end + 20);
   context.lineTo(start + 70, end + 70);
   context.lineWidth = 20;
   context.lineCap = 'round';
   context.fillStyle = 'black';
   context.fill();
   context.stroke();

   //second line for the X
   context.beginPath();
   context.moveTo(start + 70, end + 20);
   context.lineTo(start + 20, end + 70);
   context.lineWidth = 20;
   context.lineCap = 'round';
   context.fillStyle = 'black';
   context.fill();
   context.stroke();


 }
 //Draws an O in a selected box
 function drawO(start, end)
 {
   console.log("Calling draw 0");
   context.beginPath();
   context.arc(start + 40, end + 40, 40, 0, 2 * Math.PI, false);
   context.lineWidth = 15;
   context.stroke();
 }

 //a functions so the server can easily draw
 function serverDraw(cell)
 {
   var height = canvas.height;
   var width = canvas.width;
   switch (cell)
   {
     case 0:
       drawO(20, 20);
       break;
     case 1:
       drawO(width * (1 / 3) + 20, 20);
       break;
     case 2:
       drawO(width * (2 / 3) + 20, 20);
       break;
     case 3:
       drawO(20, height * (1 / 3) + 20);
       break;
     case 4:
       drawO(width * (1 / 3) + 20, height * (1 / 3) + 20);
       break;
     case 5:
       drawO(width * (2 / 3) + 20, height * (1 / 3) + 20);
       break;
     case 6:
       drawO(20, height * (2 / 3) + 20);
       break;
     case 7:
       drawO(width * (1 / 3) + 20, height * (2 / 3) + 20);
       break;
     case 8:
       drawO(width * (2 / 3) + 20, height * (2 / 3) + 20);
       break;
   }
 }
 //a functions so the client can easily draw
 function clientDraw(cell)
 {
   var height = canvas.height;
   var width = canvas.width;
   switch (cell)
   {
     case 0:
       drawX(20, 20);
       break;
     case 1:
       drawX(width * (1 / 3) + 20, 20);
       break;
     case 2:
       drawX(width * (2 / 3) + 20, 20);
       break;
     case 3:
       drawX(20, height * (1 / 3) + 20);
       break;
     case 4:
       drawX(width * (1 / 3) + 20, height * (1 / 3) + 20);
       break;
     case 5:
       drawX(width * (2 / 3) + 20, height * (1 / 3) + 20);
       break;
     case 6:
       drawX(20, height * (2 / 3) + 20);
       break;
     case 7:
       drawX(width * (1 / 3) + 20, height * (2 / 3) + 20);
       break;
     case 8:
       drawX(width * (2 / 3) + 20, height * (2 / 3) + 20);
       break;
   }
 }


 //checking with the server to see if it is a open spot
 function vaildSpot()
 {
   $.ajax({
     dataType: "json",
     type: 'GET',
     url: '/BenJohnsonQ1/ticTacToe',
     data: data,
     success: function(obj) {
       if (obj.error < 0)
       {
	 alreadyChecked(obj.error);
       }
       else
       {
	  clientDraw(data.number);
	 serverDraw(obj.move);
	 if(obj.won==="yes")
	 {
	   winnerBox(obj.message);	   
	 }

       }
     }
   });
 }

 //defineing the data object; 
 //'name' is for debugging
 var data = {number: null, name:"",checked:null};

//defening canvas element
 var canvas = document.getElementById('myCanvas');
 var context = canvas.getContext('2d');


 canvas.addEventListener('mousedown', function(evt) {
   var mousePos = getMousePos(canvas, evt);

   var height = canvas.height;
   var width = canvas.width;

   //cell8
   if (mousePos.x > width * (2 / 3) && mousePos.y > height * (2 / 3))
   {
     console.log("Cell 8");
     data.name = "cell 8";
     data.checked = 1;
     data.number = 8;

   }
   //cell 5
   else if (mousePos.x > width * (2 / 3) && mousePos.y > height * (1 / 3))
   {
     console.log("cell 5");
     data.name = "cell 5";
     data.checked = 1;
     data.number = 5;
   }
   //cell 2
   else if (mousePos.x > width * (2 / 3) && mousePos.y < height * (2 / 3))
   {
     console.log("cell 2");
     data.name = "cell 2";
     data.checked = 1;
     data.number = 2;
   }
   //cell 7
   else if (mousePos.x > width * (1 / 3) && mousePos.y > height * (2 / 3))
   {
     console.log("Cell 7");
     data.name = "cell 7";
     data.checked = 1;
     data.number = 7;
   }
   //cell 4
   else if (mousePos.x > width * (1 / 3) && mousePos.y > height * (1 / 3))
   {
     console.log("cell 4");
     data.name = "cell 4";
     data.checked = 1;
     data.number = 4;
   }
   //cell 1
   else if (mousePos.x > width * (1 / 3) && mousePos.y < height * (2 / 3))
   {
     console.log("cell 1");
     data.name = "cell 1";
     data.checked = 1;
     data.number = 1;
   }
   //cell 6
   else if (mousePos.x < width * (2 / 3) && mousePos.y > height * (2 / 3))
   {
     console.log("Cell 6");
     data.name = "cell 6";
     data.checked = 1;
     data.number = 6;
   }
   //cell 3
   else if (mousePos.x < width * (2 / 3) && mousePos.y > height * (1 / 3))
   {

     console.log("cell 3");
     data.name = "cell 3";
     data.checked = 1;
     data.number = 3;
   }
   //cell0
   else if (mousePos.x < width * (2 / 3) && mousePos.y < height * (2 / 3))
   {
     console.log("cell 0");
     data.name = "cell 0";
     data.checked = 1;
     data.number = 0;
   }

   //Making a ajax request
   vaildSpot();
 }, false);

   
 //left line
 context.beginPath();
 context.moveTo(canvas.width / 3, 18);
 context.lineTo(canvas.width / 3, canvas.height - 18);
 context.lineWidth = 20;
 context.lineCap = 'round';
 context.stroke();

 //right line
 context.beginPath();
 context.moveTo(canvas.width * (2 / 3), 18);
 context.lineTo(canvas.width * (2 / 3), canvas.height - 18);
 context.stroke();
 console.log("Called agian");
 //top line
 context.beginPath();
 context.moveTo(18, canvas.height / 3);
 context.lineTo(canvas.width - 18, canvas.height / 3);
 context.stroke();
 //bottom line
 context.beginPath();
 context.moveTo(18, canvas.height * (2 / 3));
 context.lineTo(canvas.width - 18, canvas.height * (2 / 3));
 context.stroke();

