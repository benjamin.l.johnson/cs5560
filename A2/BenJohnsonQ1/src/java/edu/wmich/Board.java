package edu.wmich;

import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ben Johnson
 */
public class Board
{

    private int board[] ={-1,-1,-1,-1,-1,-1,-1,-1,-1};
    private int cellsOpen = 0;
    private int winner = -1;

    public boolean cellEmpty(int cellNum)
    {
        return board[cellNum] < 0;
    }

    public void setCellValue(int cellNum, int cellValue)
    {
        board[cellNum] = cellValue;
        cellsOpen++;
    }

    public int getCellValue(int cellNum)
    {
        return board[cellNum];
    }

    public int calculateNextMove()
    {
        Random rng = new Random();
        int move=-1;
        int num=0;
        boolean cornner=false;
        int[] counts = new int[8];
        //look for a random cornner
        while(move<0)
        {
            num=rng.nextInt(9);
            if(board[num]<0)
            {
                move=num;
            }
        }
        
        for(int i=0;i<board.length;i++)
        {
            //even number & open
            
        
        }
        return move;
    }

    public int checkWin()
    {
        //win across top
        if (board[0] == board[1] && board[1] == board[2] && board[0] > -1)
        {

            winner = board[0];

        //win down first coloumn
        } else if (board[0] == board[3] && board[3] == board[6] && board[0] > -1)
        {

            winner = board[0];

         //win 
        } else if (board[0] == board[4] && board[4] == board[8] && board[0] > -1)
        {

            winner = board[0];
            
        } else if (board[2] == board[5] && board[5] == board[8] && board[2] > -1)
        {

            winner = board[2];

        } else if (board[2] == board[4] && board[4] == board[6] && board[2] > -1)
        {

            winner = board[2];
        } else if (board[1] == board[4] && board[4] == board[7] && board[1] > -1)
        {

            winner = board[1];
        } else if (board[3] == board[4] && board[4] == board[5] && board[3] > -1)
        {

            winner = board[3];
        } else if (board[6] == board[8] && board[8] == board[7] && board[6] > -1)
        {

            winner = board[6];
        } else if (cellsOpen > 8)
        {

            winner = 2;
        }
        


        return winner;

    }

    public int getOpenCells()
    {
        return cellsOpen;
    }

    public String getWinner()
    {
        String retval = "No one";

        if (winner == 1)
        {
            retval = "X's won!";
        } else if (winner == 0)
        {
            retval = "O's won!";
        } else if (winner == 2)
        {
            retval = "Tie!";
        } else
        {
            retval = "No one";
        }
        return retval;
    }
}
