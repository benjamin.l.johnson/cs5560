/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wmich;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author benj
 */
@WebServlet(name = "Controller", urlPatterns =
{
    "/Controller"
})
public class Controller extends HttpServlet
{

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter())
        {
            /* TODO output your page here. You may use following sample code. */

            String req = request.getParameter("page");
            
            try
            {
                int[] pages;
                req = req.toLowerCase();

                //first session
                if (request.getSession().getAttribute("pages") == null)
                {
                    pages = new int[4];
                    pages[0] = 0;
                    pages[1] = 0;
                    pages[2] = 0;
                    pages[3] = 0;
                } else
                {
                    //casting to a int array 
                    pages = (int[]) request.getSession().getAttribute("pages");
                }

                switch (req)
                {
                    case "stats":
                        pages[0]++;
                        break;
                        
                    case "page1":
                        pages[1]++;
                        break;
                        
                    case "page2":
                        pages[2]++;
                        break;
                        
                    case "page3":
                        pages[3]++;
                        break;
                        
                    default:
                        req = "index";
                        break;

                }
                request.getSession().setAttribute("pages", pages);
                //Setting the experation date so browser does not cache the page
                response.setDateHeader("Expires", 0);
                
                if(req.equals("stats"))
                {
                    request.getRequestDispatcher(req + ".jsp").forward(request, response);
                }
                else
                {
                    request.getRequestDispatcher(req + ".html").forward(request, response);
                }
            } catch (NullPointerException e)
            {
                request.getRequestDispatcher("index.html").forward(request, response);
            }


        }


    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

}
