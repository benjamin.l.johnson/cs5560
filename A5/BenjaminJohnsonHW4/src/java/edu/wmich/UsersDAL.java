/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wmich;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;

/**
 *
 * @author benj
 */
@Named(value = "UsersDAL")
@SessionScoped
public class UsersDAL implements Serializable
{

	@PersistenceUnit
	EntityManagerFactory enf;

	@Resource
	UserTransaction utx;

	private String userName;
	private String password;
	private boolean authed = false;

	/**
	 * Creates a new instance of studentDAL
	 */
	public UsersDAL()
	{


	}

	public String login()
	{
		UsersJpaController apajc = new UsersJpaController(utx, enf);
		try
		{
			if (!authed)
			{
				Users user = apajc.findUsers(userName);
				System.err.println("\nUsername: "+ userName +"\nPassWord: "+password);
				if(user == null)
				{
					
					return null;
				}
				
				if (user.getPassword().equals(password))
				{
					System.err.println("authed");
					authed = true;
					return "/view";
				} else
				{
					System.err.println("not authed");
					authed = false;
					return null;
				}
			} else
			{
				return "/view";
			}
		} catch (Exception e)
		{

			return "index";
		}

	}

	public String logOut()
	{
		authed = false;
		return "/index";
	}

	public List getAllUsers()
	{
		UsersJpaController apajc = new UsersJpaController(utx, enf);

		/*
		 Students st = new Students(12222,"james","jones","james.j@aol.com");
		 */

		try
		{
			return apajc.findUsersEntities();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

}
