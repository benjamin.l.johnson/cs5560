/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import edu.wmich.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author happy
 */
@Stateless
@Path("edu.wmich.users")
public class UsersFacadeREST extends AbstractFacade<Users>
{
	@PersistenceContext(unitName = "BenJohnsonHW5Q2PU")
	private EntityManager em;

	public UsersFacadeREST()
	{
		super(Users.class);
	}


	@Override
	protected EntityManager getEntityManager()
	{
		return em;
	}
	
}
