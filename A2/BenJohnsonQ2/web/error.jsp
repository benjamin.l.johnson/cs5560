<%-- 
    Document   : error
    Created on : May 18, 2014, 10:25:46 PM
    Author     : BenJohnson
    A simple error page to tell you that you did not select a radio button
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Error</title>
    </head>
    <body>
        <h1>You Gave me bad input!</h1>
        <h3>Each button must be selected and you need to have a number in the input box</h3>
        <form action="view.jsp">
            <button type="submit" class="btn btn-primary btn-lg">Try Again?</button>
        </form>
    </body>
</html>
