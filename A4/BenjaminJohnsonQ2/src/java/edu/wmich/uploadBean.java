/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wmich;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.primefaces.event.FileUploadEvent;


/**
 *
 * @author Ben Johnson
 */
@Named(value = "uploadBean")
@SessionScoped
public class uploadBean implements Serializable
{
    //used alot only need to change once this way
    private final String imagePath = "/home/happy/Programs/cs5560/A4/BenjaminJohnsonQ2/web/images/";
    private String title;
    private String path;
    private Image image;
    private ArrayList<Image> images = new ArrayList<Image>();
    
    
    public uploadBean()
    {
        //first time this is called lets add all the files alread in the directory 
        File folder = new File(imagePath);
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles)
        {
            if (file.isFile())
            {
                images.add(new Image("../images/"+file.getName(), file.getName()));
            }
        }
    }

    /**
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * @return the path
     */
    public String getPath()
    {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path)
    {
        this.path = path;
    }

    /**
     * @return the image
     */
    public Image getImage()
    {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(Image image)
    {
        this.image = image;
    }

    /**
     * @return the images
     */
    public ArrayList<Image> getImages()
    {
        return images;
    }

    /**
     * @param images the images to set
     */
    public void setImages(ArrayList<Image> images)
    {
        this.images = images;
    }

    public void upload(FileUploadEvent event)
    {
        try
        {
            title = event.getFile().getFileName();
            File targetFolder = new File(imagePath);
            InputStream inputStream = event.getFile().getInputstream();
            OutputStream out = new FileOutputStream(new File(targetFolder, title));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1)
            {
                out.write(bytes, 0, read);
            }
            inputStream.close();
            out.flush();
            out.close();
            path = "../images/" + title;
            System.err.println(path);
            images.add(new Image(path, title));
            
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        
    }
}
