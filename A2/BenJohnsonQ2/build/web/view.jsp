<%-- 
    Document   : view
    Created on : May 18, 2014, 3:14:58 PM
    Author     : Benjamin Johnson

    I use two for loops to iterate through the radio buttons
    I then look for the convert object if it is not stored in the session I make one 
    to display some output instead of null
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="edu.wmich.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Benjamin Johnson's Q2</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>


    </head>
    <body>
        <div class="navbar-inverse">
         <ul class="nav nav-tabs">
            <li class="navbar-brand">Q2</li>
            <li><a href="#/calc">Temp Converter</a></li>
        </ul>
    </div>
        <%-- Setting the types right so I can iterate over them later --%>
        <% String[] types= { "Celsius","Fahrenheit","Kelvin","Rankine"};
        
        %>
    <br>
       <form action="/BenJohnsonQ2/controller" method="GET">
    <div class="col-lg-4 ">
        <div class="has-warning">
        <label>Convert</label>
        <input class="form-control" style="background-color:#E2F754;" name="num" value="0" type="number"/> 
        </div>
        <div class="row">
            <div class="col-lg-5">
                <label>From</label>
                <% for(String type: types){ 
                %>
                <div class="radio" >
                    
                    <label>
                        <%= type %>
                        <input type="radio" name="from" value="<%=type.substring(0,1)%>"/>
                    </label>
                
                </div> <% } %>
            </div>

            <div class="col-lg-5">
                <label>To</label>
                
                <% for(String type: types){ %>
                <div class="radio">
                    <label>
                        <%= type %>
                        <input type="radio" name="to" value="<%=type.substring(0,1) %>"/>
                    </label>
                </div> <% } %>
            </div>
        </div>
        <div class="row">
            <div class=" col-lg-offset-2">
                <div>
                    <% 
                    Converter converter;
                    if(request.getSession().getAttribute("converter") !=null)
                    {
                      converter =(Converter) request.getSession().getAttribute("converter");
                    }
                    else
                    {
                       converter=new Converter();
                    }
                    
                    %>
                    <h3><%=converter.getFinalResult()%></h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-offset-2">
                <output><%=converter.getEqn()%></output>
            </div>
        </div>
            <br>
           <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
      
    </div>
</form> 

    <div class="navbar-fixed-bottom" >
        <ul class="navbar-brand">
            <li class="navbar-brand">Ben Johnson</li>
        </ul>
    </div>
    </body>
</html>
