/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wmich;


import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;


/**
 *
 * @author happy
 */
@Named(value = "weatherBean")
@SessionScoped
public class WeatherBean implements Serializable
{

	private int zip;
	private String temp;

	public WeatherBean()
	{
	}

	/**
	 * @return the zip
	 */
	public int getZip()
	{
		return zip;
	}

	/**
	 * @param zip the zip to set
	 */
	public void setZip(int zip)
	{
		this.zip = zip;
	}

	/**
	 * @return the temp
	 */
	public String getTemp()
	{

		return temp;
	}

	/**
	 * @param temp the temp to set
	 */
	public void setTemp(String temp)
	{
		this.temp = temp;
	}

	public String currentTemp()
	{
		String key = "6b286f19fe6fa1cd5baba60e17e83487fd274f80";
		String serviceName = "http://api.worldweatheronline.com/free/v1/weather.ashx?key=";
		String format = "&format=json";

		Weather w = new Weather(key,serviceName,String.valueOf(zip), format);
		temp = w.getCurrentTemp();
		return "temp";
	}

}
