/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wmich;

import edu.wmich.exceptions.RollbackFailureException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author happy
 */
@Path("links")
public class LinksRest
{
	@PersistenceUnit(unitName = "BenjaminJohnsonHW4PU")
	EntityManagerFactory enf;

	@Resource
	UserTransaction utx;

	@Context
	private UriInfo context;

	/**
	 * Creates a new instance of LinksResources
	 */
	public LinksRest()
	{
	}

	@PUT
	@Path("create/{siteLogin}/{sitePassword}/{url}/{userName}/{password}")
	@Consumes(
			{
				"application/json"
			})
	public void create(@PathParam("url") String url, @PathParam("userName") String userName, 
			@PathParam("password") String password,@PathParam("siteLogin") String siteLogin, @PathParam("sitePassword") String sitePassword)
	{
		if(isAuthed(siteLogin, sitePassword))
		{
			LinksJpaController lpajc = new LinksJpaController(utx, enf);

			try
			{
				Links l = new Links();
				l.setPassword(password);
				l.setUrl(url);
				l.setUserName(userName);

				lpajc.create(l);
			} catch (RollbackFailureException ex)
			{
				Logger.getLogger(LinksRest.class.getName()).log(Level.SEVERE, null, ex);
			} catch (Exception ex)
			{
				Logger.getLogger(LinksRest.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	@DELETE
	@Path("{siteLogin}/{sitePassword}/{id}")
	public void remove(@PathParam("id") String id,@PathParam("siteLogin") String login, @PathParam("sitePassword") String passwd)
	{
		if (isAuthed(login, passwd))
		{
			LinksJpaController lpajc = new LinksJpaController(utx, enf);
			try
			{
				lpajc.destroy(id);
			} catch (RollbackFailureException ex)
			{
				Logger.getLogger(LinksRest.class.getName()).log(Level.SEVERE, null, ex);
			} catch (Exception ex)
			{
				Logger.getLogger(LinksRest.class.getName()).log(Level.SEVERE, null, ex);
			}
		} else
		{
			loginError();
		}

	}

	@GET
	@Path("{siteLogin}/{sitePassword}")
	@Produces(
			{
				"application/json"
			})
	public List<Links> findAll(@PathParam("siteLogin") String id, @PathParam("sitePassword") String passwd)
	{
		if (isAuthed(id,passwd))
		{
			LinksJpaController lpajc = new LinksJpaController(utx, enf);
			return lpajc.findLinksEntities();
		} else
		{
			loginError();
		}
		return null;
	}

	private void loginError()
	{
		String errorMsg = "{\"error\":\"need to login\"}";
		throw new NotAuthorizedException(Response.status(Response.Status.UNAUTHORIZED).entity(errorMsg).build());
	}

	private boolean isAuthed(String siteLogin, String sitePasswd)
	{
		UsersJpaController apajc = new UsersJpaController(utx, enf);

		try
		{
			Users user = apajc.findUsers(siteLogin);
			if (user == null)
			{

				return false;
			}

			if (user.getPassword().equals(sitePasswd))
			{
				System.err.println("authed");
				return true;
			} else
			{
				System.err.println("not authed");
				return false;
			}
		} catch (Exception e)
		{

			return false;
		}
	}


}
