/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.wmich;

import java.io.Serializable;
import java.util.ArrayList;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author benj
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable{

    /**
     * Creates a new instance of LoginBean
     */
    
    private String userName;
    private String userPass;
    private String vaildUserName="test";
    private final String vaildUserPass="test";
    private boolean authed=false;
    
    private ArrayList<Value> allValues = new ArrayList<Value>();
    private String url;
    private String passwd;
    private String icon;
    private String siteUserName;
    
    
    
    
    public LoginBean() {
       
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the userPass
     */
    public String getUserPass() {
        return userPass;
    }

    /**
     * @param userPass the userPass to set
     */
    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }


    /**
     * @return the vailsUserPass
     */
    public String getVaildUserPass() {
        return vaildUserPass;
    }

    /**
     * @return the authed
     */
    public boolean isAuthed() {
        return authed;
    }

    /**
     * @param authed the authed to set
     */
    public void setAuthed(boolean authed) {
        this.authed = authed;
    }
    public String login()
    {
        if(userName.equals(vaildUserName) && userPass.equals(vaildUserPass)){
            authed=true;
            return "main";
        }
        else{
            authed=false;
            return "login";
        }
    }
    public String logOut()
    {
        authed = false;
        return "login";
    }

    /**
     * @return the allValues
     */
    public ArrayList<Value> getAllValues()
    {
        return allValues;
    }

    /**
     * @param allValues the allValues to set
     */
    public void setAllValues(ArrayList<Value> allValues)
    {
        this.allValues = allValues;
    }

    /**
     * @return the url
     */
    public String getUrl()
    {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url)
    {
        this.url = url;
    }

    /**
     * @return the passwd
     */
    public String getPasswd()
    {
        return passwd;
    }

    /**
     * @param passwd the passwd to set
     */
    public void setPasswd(String passwd)
    {
        this.passwd = passwd;
    }

    /**
     * @return the icon
     */
    public String getIcon()
    {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(String icon)
    {
        this.icon = icon;
    }
    public String deleteValue(Value v)
    {
        allValues.remove(v);
        return "main";
    }
    public String addValue(){
        allValues.add(new Value(url,passwd,siteUserName));
        return "main";
    }

    /**
     * @return the siteUserName
     */
    public String getSiteUserName()
    {
        return siteUserName;
    }

    /**
     * @param siteUserName the siteUserName to set
     */
    public void setSiteUserName(String siteUserName)
    {
        this.siteUserName = siteUserName;
    }


}
