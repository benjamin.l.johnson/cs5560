'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
        .controller('MyCtrl1', ['$scope', function($scope) {

                //array for expressions
                $scope.expressions = [
                    {name: 'Add', id: 'add'},
                    {name: 'Subtract', id: 'sub'},
                    {name: 'Multiply', id: 'mul'},
                    {name: 'Divide', id: 'div'}
                ];

                //setting background color
                $scope.inp = [
                    {'background-color': '#E2F754', id: 0},
                    {'background-color': '#E2F754', id: 1}
                ];

                //changing id based tid (passed in from html)
                $scope.change = function(tid)
                {
                    $scope.inp[tid] = {'background-color': '#F05672', id: tid};
                };


                $scope.select = function(tid)
                {

                    if (tid == 'add')
                    {
                        $scope.num3 = $scope.num1 + $scope.num2;
                    }
                    else if (tid == 'sub')
                    {
                        $scope.num3 = $scope.num1 - $scope.num2;
                    }
                    else if (tid == 'mul')
                    {
                        $scope.num3 = $scope.num1 * $scope.num2;
                    }
                    else if (tid == 'div')
                    {
                        $scope.num3 = $scope.num1 / $scope.num2;
                    }
                    if ($scope.num3 === null || angular.isUndefined($scope.num3))
                    {
                        $scope.num3 = "error";
                    }
                }
            }])
        .controller('MyCtrl2', ['$scope', function($scope) {

            }]);
