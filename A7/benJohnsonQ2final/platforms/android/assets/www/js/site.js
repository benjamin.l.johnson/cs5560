 /* 
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */


//global price variable
 var lastPrice = '';
 
 //global authed variable
 var authed = false;
 document.addEventListener("deviceready", onDeviceReady, true);

//function to exit app
 function exitApplic() {
   if (navigator.app) {
     navigator.app.exitApp();
   }
   else if (navigator.device) {
     navigator.device.exitApp();
   }
 }
 
 
 function onDeviceReady() {
   app.receivedEvent('deviceready');
   $.mobile.changePage("#login");
 }
 
 //when they tap the login buttton
 $(document).on("vclick", "#login", function() {
   if (localStorage.userName == null) {
     localStorage.setItem('userName', $('#userName').val());
     localStorage.setItem('password', $('#password').val());
     authed = true;
     $.mobile.navigate("#page1");
   }
   else
   {
     if (localStorage.userName == $('#userName').val()
	     && localStorage.password == $('#password').val())
     {
       authed = true;
       $.mobile.navigate("#page1");
     }
     else
     {
       alert("Bad login try agian");
     }
   }
 });

 $(document).on('pagebeforechange', function(e, data) {
   var to = data.toPage;
   if (typeof to === 'string') {
     var u = $.mobile.path.parseUrl(to);
     to = u.hash || '#' + u.pathname.substring(1);

     if (to === '#page1' && !authed) {
       alert('Must login first');
       e.preventDefault();
       e.stopPropagation();
       // remove active status on a button, if transition was triggered with a button
       $.mobile.activePage.find('.ui-btn-active').removeClass('ui-btn-active ui-shadow').css({'box-shadow': '0 0 0 #3388CC'});
     }
   }
 });
 $(document).on("vclick", "#lgout", function() {
   authed = false;
   $.mobile.navigate("#loginPage");
   exitApplic();
 });
 $(document).on("tap", "#getPrice", function() {

   var url = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%3D%22' + $('#ticker').val() + '%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=';

   if ($('#ticker').val() !== "") {
     $.ajax({
       beforeSend: function() {
	 $.mobile.loading('show');
       }, //Show spinner
       complete: function() {
	 $.mobile.loading('hide');
       }, //Hide spinner
       url: url,
       dataType: 'json',
       success: function(data) {
	 lastPrice = data.query.results.quote.LastTradePriceOnly;

	 $('#tickerData').empty();
	 $('#tickerData').append('<h2>Last know Price for ' + $('#ticker').val() + ' is ' + lastPrice + '</h2>');
	 $('#tickerpopup').popup("open");
	 $('#ticker').val("");
       },
       error: function(jqXHR) {
	 alert("Error " + jqXHR.responseText);
	 console.log(jqXHR.responseText);
       }
     });
   }
   else
   {
     alert("Need input");
   }
 });
