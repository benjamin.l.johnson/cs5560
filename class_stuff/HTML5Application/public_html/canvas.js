 /* 
  * Each box for the board is labled accordinally 
  * 0 1 2 
  * 3 4 5
  * 6 7 8
  * 
  */
 function getMouse(canvas, message) {
   var context = canvas.getContext('2d');
 }
 function getMousePos(canvas, evt) {
   var rect = canvas.getBoundingClientRect();

   //call to the draw method

   return {
     x: evt.clientX - rect.left,
     y: evt.clientY - rect.top
   };
 }

 //each function will start at the corner
 function drawX(start, end)
 {
   //
   context.moveTo(start + 20, end + 20);
   context.lineTo(start + 70, end + 70);
   context.lineWidth = 20;
   context.lineCap = 'round';
   context.stroke();

   context.moveTo(start + 70, end + 20);
   context.lineTo(start + 20, end + 70);
   context.stroke();
 }
 function drawO(start, end, cell)
 {
   context.beginPath();
   context.arc(start + 40, end + 40, 40, 0, 2 * Math.PI, false);
   context.fillStyle = 'green';
   context.fill();
   context.lineWidth = 5;
   context.strokeStyle = '#003300';
   context.stroke();
 }
 function serverDraw()
 {
   
 }

 var canvas = document.getElementById('myCanvas');
 var context = canvas.getContext('2d');
 var cells = [-1, -1, -1, -1, -1, -1, -1, -1, -1];

 canvas.addEventListener('mousedown', function(evt) {
   var mousePos = getMousePos(canvas, evt);
   var message = 'Mouse position: ' + mousePos.x + ',' + mousePos.y;
   //console.log(message);
   var height = canvas.height;
   var width = canvas.width;
   //cell8
   if (mousePos.x > width * (2 / 3) && mousePos.y > height * (2 / 3) && cells[8]<0)
   {
     console.log("Cell 8");
     drawX(width * (2 / 3) + 20, height * (2 / 3) + 20);
     cells[8]=1;
     //drawO(width * (2 / 3) + 20, height * (2 / 3) + 20);

   }
   //cell 5
   else if (mousePos.x > width * (2 / 3) && mousePos.y > height * (1 / 3) && cells[5]<0)
   {
     console.log("cell 5");
     drawX(width * (2 / 3) + 20, height * (1 / 3) + 20);
     cells[5]=1;
     //drawO(width * (2 / 3) + 20, height * (1 / 3) + 20);
   }
   //cell 2
   else if (mousePos.x > width * (2 / 3) && mousePos.y < height * (2 / 3) && cells[2]<0)
   {
     console.log("cell 2");
     drawX(width * (2 / 3) + 20, 20);
     cells[2]=1;
     //drawO(width * (2 / 3) + 20 , 20);
   }
   //cell 7
   else if (mousePos.x > width * (1 / 3) && mousePos.y > height * (2 / 3) && cells[7]<0)
   {
     console.log("Cell 7");
     drawX(width * (1 / 3) + 20, height * (2 / 3) + 20);
     cells[7]=1;
     //drawO(width * (1 / 3) + 20, height * (2 / 3) + 20);
   }
   //cell 4
   else if (mousePos.x > width * (1 / 3) && mousePos.y > height * (1 / 3) && cells[4]<0)
   {
     console.log("cell 4");
     drawX(width * (1 / 3) + 20, height * (1 / 3) + 20);
     cells[4]=1;
     //drawO(width * (1 / 3) + 20, height * (1 / 3) + 20);
   }
   //cell 1
   else if (mousePos.x > width * (1 / 3) && mousePos.y < height * (2 / 3) && cells[1]<0)
   {
     console.log("cell 1");
     drawX(width * (1 / 3) + 20, 20);
     cells[1]=1;
     //drawO(width * (1 / 3) + 20, 20);
   }
   //cell 6
   else if (mousePos.x < width * (2 / 3) && mousePos.y > height * (2 / 3) && cells[6]<0)
   {
     console.log("Cell 6");
     drawX(20, height * (2 / 3) + 20);
     cells[6]=1;
     //drawO( 20, height * (2 / 3) + 20);
   }
   //cell 3
   else if (mousePos.x < width * (2 / 3) && mousePos.y > height * (1 / 3) && cells[3]<0)
   {
     console.log("cell 3");
     drawX(20, height * (1 / 3) + 20);
     cells[3]=1;
     //drawO( 20, height * (1 / 3) + 20);
   }
   //cell0
   else if (mousePos.x < width * (2 / 3) && mousePos.y < height * (2 / 3) && cells[0]<0)
   {
     console.log("cell 0");
     drawX(20, 20);
     cells[0]=1;
     //drawO(20,  20);
   }

 }, false);


 //left line
 context.beginPath();

 //a padding element
 //var bord = ((canvas.height+canvas.width)*(1/32));
 //console.log(bord);
 context.moveTo(canvas.width / 3, 18);
 context.lineTo(canvas.width / 3, canvas.height - 18);
 context.lineWidth = 20;
 context.lineCap = 'round';
 context.stroke();

 //right line
 context.beginPath();
 context.moveTo(canvas.width * (2 / 3), 18);
 context.lineTo(canvas.width * (2 / 3), canvas.height - 18);
 context.stroke();


 //top line
 context.beginPath();
 context.moveTo(18, canvas.height / 3);
 context.lineTo(canvas.width - 18, canvas.height / 3);
 context.stroke();

 //bottom line
 context.beginPath();
 context.moveTo(18, canvas.height * (2 / 3));
 context.lineTo(canvas.width - 18, canvas.height * (2 / 3));
 context.stroke();




